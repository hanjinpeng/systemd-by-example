# Using the Systemd Event Loop and the Systemd Dbus Library

**Note:** The files for this example can be found in the GitLab repository for the book
[^book-repository] in the directory **examples/example-sd-event-and-sd-bus**.

Systemd provides two libraries that build the basis for the implementation of systemd tools
itself, namely **sd-event** [^man-systemd-sd-event] an event loop based on **epoll** and
**sd-bus** [^man-systemd-sd-bus] a DBus library. Lennart Poettering has an introduction to
**sd-event** [^pid-eins-sd-event] and **sd-bus** [^pid-eins-sd-bus] on his blog. In this
section I will build on his examples and implement a systemd daemon using the systemd
libraries **sd-daemon**, **sd-event** and **sd-bus** all in combination with each other.

In the following two sections I will first describe how to implement a very simple program
usind **sd-event** and in the following section I will extend the program to handle messages
from DBus.


## Systemd Event Loop **sd-event**

A typical application based on **sd-event** first created the event loop using
one of **sd_event_net()** or **sd_event_default()** functions described in the man pages of
**sd_event_default** [^man-systemd-sd-event-default].

Once the event loop is created, the application registers event handler for the event
sources it wants to handle. The systemd event loop handles events from the following
sources for which the event handler will be registered with different registration
functions.

 - I/O events
 
    I/O events are handled using the **epoll** mechanism of the linux kernel and monitor a
    file described by a file descriptor. The file descriptor can represent a socket, a FIFO,
    a message queue, a serial connection, a character device or any other file descriptor
    compatible with **epoll**
   
    I/O events are registered using **sd_event_add_io()** [^man-systemd-sd-event-add-io].
   
 - Timer events
 
    The event loop can handle timer events with different resolution and accuracy. Timer
    events are registered using function **sd_event_add_time** [^man-systemd-sd-event-add-time].
   
 - Unix process signal events
 
    The event loop can handle unix process signals. Unix process signals are registered using
    the function **sd_event_add_signal** [^man-systemd-sd-event-add-signal].
   
 - Child process state events
 
    The event loop can listen to state changes of child processes. Handler for events of that
    type are scheduled using **sd_event_add_child** [^man-systemd-sd-event-add-child].
   
 - Inotify file system inode events
 
    The event loop can listen to inotify inode events [^man-linux-inotify] which can be
    registered using **sd_event_add_inotify** [^man-systemd-sd-event-add-inotify].
 
 - Static event sources
 
    The event loop can handle the following static event types.
     - Defer
    
        Defers an event for immediate execution in the next loop run, registered using
        **sd_event_add_defer** [^man-systemd-sd-event-add-defer].
    
     - Post
    
        Defers an event for execution in the next event loop run but only if no other event
        non-post event is available. Registered using **sd_event_add_post**
        [^man-systemd-sd-event-add-post].
      
     - Exit
    
        Will be executed when the event loop is terminated using **sd_event_exit**. The
        functions are registered using **sd_event_add_exit** [^man-systemd-sd-event-add-exit].

After the event handler are registered, the application starts the event loop using
**sd_event_loop** [^man-systemd-sd-event-loop]. The example I will present in the following
part does not really do anything exiting. It only sets up the event loop and registers handling
for the following unix signals to build a very simple basic event loop for an **sd-event**
based application.

 - SIGTERM [^man-linux-signal]
 
    The termination signal, signals that the process should be terminated.
   
 - SIGINT [^man-linux-signal]

    The keyboard interrupt signal, signals that the process should be interrupted.

The event loop provides default handling for both of these signals and I will use the default
handling in the sample application. The source code for the event loop is shown below.

!INCLUDECODE "examples/example-sd-event-and-sd-bus/sd_event_loop_example.cpp" (cpp)

Running the sample application produces the following output, indicating that the systemd
event loop is running and processing events.

```
Creating event loop
Clocking signals
Adding signal handler
Starting event loop
```

Because there are no events that the event loop processes except for the unix signals I
added handler for, the output does not show anything interesting. Adding a timer event to
the event loop using **sd_event_add_time()** to output something on standard output every
few seconds will change that.

!INCLUDECODE "examples/example-sd-event-and-sd-bus/sd_event_loop_with_timer_example.cpp" (cpp)

The function to add a timer event **sd_event_add_time()** takes an absolute time value in
microseconds as parameter. To calculate the wakeup time I retrieve the time using
**sd_event_now()** and add 1 second (1000000 microseconds) to the current time. Then I
schedule the timer to call the callback **timerCallback** at the calculated wakeup time.
Additionally, I set the timer event to be a repeated event, because the timer events are one
shot events by default. The timer handler calculates a new wake up time and updates the time
of the timer event so that the timer is triggered again after one second. Running the
application now yields the following output adding "Timer was triggered" every second.

```
Creating event loop
Clocking signals
Adding signal handler
Starting event loop
Timer was triggered
Timer was triggered
Timer was triggered
Timer was triggered
Timer was triggered
Timer was triggered
Timer was triggered
Timer was triggered
```


## Systemd DBus library **sd-bus**

An application implementing a D-Bus api exposes interfaces implemented as objects epxosed by a
service on a service bus. D-Bus is specified in the D-Bus Specification [^dbus-specification]
and guidelines for designing a D-Bus api can be found in the D-Bus API Design Guidelines
[^dbus-design-guidelines]. Systemd uses D-Bus heavily to allow the control of systemd itself
as well as the daemons provided by systemd and also provides its own api to implement D-Bus apis
**sd-bus** integrated with **sd-event** and provides a nice c api to implement D-Bus apis.

Lennart Poettering provides a short introduction to **sd-bus** on his blog [^pid-eins-sd-bus]
which I use as a basis to demonstrate how **sd-bus** can be integrated with **sd-event**.

An implementation of a D-Bus api using **sd-bus** typically connects to the bus and then
describes the implementation of the provided objects using an object v-table. The bus connection
can either be creted using **sd_bus_open_system()**, which connects to the system bus, or
**sd_bus_open_user()**, which connects to the user bus. I decided to use **sd_bus_open_user()**
for this example to prevent any permission problems when executing the code.

V-tables assign the object's methods and properties to calllback function. Additionally, they
store the name and the signature of methods, properties and signals. V-tables are created as
array using the special macros defined in **sd-bus-vtable.h** [^header-sd-bus-vtable-h] which
in turn is imported by **sd-bus.h**. Unfortunately there currently is no man page describing
the construction of v-tables so I'll do my best to describe constructing v-tables in the
following paragraphs. For the sake of completeness I have to mention that v-tables are not the
only way to create D-Bus objects, it is also possible to assign an object directly to a
callback using **sd_bus_add_object**. I find it preferable to create objects using v-tables
because **sd-bus** handles the multiplexing of the object's methods and properties and, even
better, creates all the necessary data for the D-Bus introspection.

As mentioned above, v-tables are constructed as array using the following macros.

 - **SD_BUS_VTABLE_START**
 
    Signals the start of the v-table.
   
 - **SD_BUS_METHOD_WITH_OFFSET**
 
    Creates a method for the object. The parameters are the name of the method, the signature of
    the input parameters, the signature of the output parameters, the callback function, flags and
    an offset parameter. The offset parameter is used to move the pointer to the user data which
    is registered with **sd_bus_add_object_vtable** to allow more generic implementations.
   
 - **SD_BUS_METHOD**
 
    Creates a method for the object. The parameters are the name of the method, the signature of
    the input parameters, the signature of the output parameters, the callback function and flags.
   
 - **SD_BUS_SIGNAL**
 
    Describes a signal for the objects. The parameters are the name of the signal, the signature
    of the signal and flags.
   
 - **SD_BUS_PROPERTY**
 
    Creates a read-only property for the object. The parameters are the name of the property, the
    signature of the property, the get handler, an offset for generic implementations and flags.
 
 - **SD_BUS_WRITABLE_PROPERTY**
 
    Create a property for the object. The parameters are the name of the property, the signature
    of the property, the get handler, the set handler, an offset parameters and flags.
   
 - **SD_BUS_VTABLE_END**

    Signals the end of the v-table.
   
The handler for methods have the following signature.

```cpp
typedef int (*sd_bus_message_handler_t)(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
```

The handler for getter have the following signature.

```cpp
typedef int (*sd_bus_property_get_t) (sd_bus *bus, const char *path, const char *interface, const char *property, sd_bus_message *reply, void *userdata, sd_bus_error *ret_error);
```

And the handler for setter have the following signature.

```cpp
typedef int (*sd_bus_property_set_t) (sd_bus *bus, const char *path, const char *interface, const char *property, sd_bus_message *value, void *userdata, sd_bus_error *ret_error);
```

Using these macros **sd-bus** allows the definition of any object legal D-Bus object. After the
v-table is created, the actual object is registered using **sd_bus_add_object_vtable()**. Once
the object is described, the object name has to be requested using **sd_bus_request_name()**
[^man-sd-bus-request-name]. When all this is done, I instruct the systemd event bus **sd-event**
to handle **sd-bus** using the function **sd_bus_attach_event()**.

For the example I implement the object *Counter* with the method *IncrementBy* and the writable
property *Count*. The method *IncrementBy* increments the *Count* by the integer supplied and
setting the property *Count* sets the *Count* to the supplied value.

Enhancing the service implementation from earlier with the D-Bus object yields the following.

!INCLUDECODE "examples/example-sd-event-and-sd-bus/sd_event_loop_with_sd_bus_example.cpp" (cpp)

Starting the application registers the D-Bus object with the service bus. Systemd provides the
tool `busctl` to monitor and test applications connected to the service bus. Calling
`busctl --user` shows all D-Bus services currently connected to the service bus and includes the
following line for the service registered by the sample application.

```
net.franksreich.Counter    27338 sd_event_loop_w frank    :1.225        session-2.scope
```

D-Bus opens up the services for introspection, the following command will display all methods
and properties for the service along with the implemented interfaces.

```
busctl --user introspect net.franksreich.Counter /net/franksreich/Counter
```

The output for the sample service looks similar to the following.

```
NAME                                TYPE      SIGNATURE RESULT/VALUE FLAGS
net.franksreich.Counter             interface -         -            -
.IncrementBy                        method    u         u            -
.Count                              property  u         0            writable
org.freedesktop.DBus.Introspectable interface -         -            -
.Introspect                         method    -         s            -
org.freedesktop.DBus.Peer           interface -         -            -
.GetMachineId                       method    -         s            -
.Ping                               method    -         -            -
org.freedesktop.DBus.Properties     interface -         -            -
.Get                                method    ss        v            -
.GetAll                             method    s         a{sv}        -
.Set                                method    ssv       -            -
.PropertiesChanged                  signal    sa{sv}as  -            -
```

The output shows that the object has the property and the method I implemented. Additionally,
**sd-bus** did a lot of work. It provided implementations for the interfaces
*org.freedesktop.DBus.Introspectable*, allowing us to introspect the service using D-Bus tools
like `busctl`, *org.freedesktopt.DBus.Peer* and *org.freedesktop.DBus.Properties*.

I can now call the D-Bus method with the following `busctl` call.

```
busctl --user call net.franksreich.Counter /net/franksreich/Counter net.franksreich.Counter IncrementBy u 5
```

Checking the counter with
´busctl --user introspect net.franksreich.Counter /net/franksreich/Counter` yields the following.

```
NAME                                TYPE      SIGNATURE RESULT/VALUE FLAGS
net.franksreich.Counter             interface -         -            -
.IncrementBy                        method    u         u            -
.Count                              property  u         5            writable
org.freedesktop.DBus.Introspectable interface -         -            -
.Introspect                         method    -         s            -
org.freedesktop.DBus.Peer           interface -         -            -
.GetMachineId                       method    -         s            -
.Ping                               method    -         -            -
org.freedesktop.DBus.Properties     interface -         -            -
.Get                                method    ss        v            -
.GetAll                             method    s         a{sv}        -
.Set                                method    ssv       -            -
.PropertiesChanged                  signal    sa{sv}as  -            -
```

And I can see that the property *Count* was incremented by 5. The implementation also allows
setting the property using `busctl` with the following.

```
busctl --user set-property net.franksreich.Counter /net/franksreich/Counter net.franksreich.Counter Count u 13
```

Reading the property with the introspect call shows that the property was changed to 13.

```
NAME                                TYPE      SIGNATURE RESULT/VALUE FLAGS
net.franksreich.Counter             interface -         -            -
.IncrementBy                        method    u         u            -
.Count                              property  u         13           writable
org.freedesktop.DBus.Introspectable interface -         -            -
.Introspect                         method    -         s            -
org.freedesktop.DBus.Peer           interface -         -            -
.GetMachineId                       method    -         s            -
.Ping                               method    -         -            -
org.freedesktop.DBus.Properties     interface -         -            -
.Get                                method    ss        v            -
.GetAll                             method    s         a{sv}        -
.Set                                method    ssv       -            -
.PropertiesChanged                  signal    sa{sv}as  -            -
```

The application allows to increment and set the counter. The D-Bus specification defines several standard
Interfaces which **sd-bus** either implements or provides functions to ease the implementation of. The
standard interfaces are:

 - **org.freedesktop.DBus.Introspectable**
 
    The introspection interface allows the introspection of D-Bus apis connected to the bus. I already used
    the introspection api when using `busctl` to connect to the service bus, **sd-bus** created the required
    introspection data based on the v-table registered for the exposed objects.
   
 - **org.freedesktop.DBus.Peer**
 
    The peer api allows to check on which machine the service runs and allows to ping the service. The
    interface is automatically created by **sd-bus**. Because the functionality is very simple I will not
    explore the feature further.

 - **org.freedesktop.DBus.Properties**
 
    The properties interface allows accessing the properties of the object. **Sd-bus** implements the method
    of the interface automatically. It does not implement the signal *PropertiesChanged* because **sd-bus**
    cannot know how the property is represented and if it changed. I will show how to signal a change in the
    property value in the next section.
   
 - **org.freedesktop.DBus.ObjectManager**
 
    The object manager interface allow to retrieve all objects of a service as well as defines signals that
    indicate if an interface is added or removed from an object. The object manager does not emit signals
    for property changes in existing objects, this is what the properties interface should be used for, but
    it emits signals for interfaces that are added or removed if asked to do so using
    **sd_bus_emit_object_added()** or **sd_bus_emit_object_removed()**. Using this interface in combination
    with the properties interface allows the client to monitor the service regarding all changes. I will
    discuss the interface in the next section.
 
 
## The Properties and ObjectManager Interfaces

The object manager is created using **sd_bus_add_object_manager()**. The function takes the path as
argument and **sd-bus** creates the object manager for that path. The created object manager can
handle the method **GetManagedObjects** immediately. It does not automatically emit the signals
**InterfacesAdded** or **InterfacesRemoved**. To emit the signals, the application has to call
**sd_bus_emit_object_added** or **sd_bus_emit_object_removed** respectively. I enhanced the service
in such a way that it exposes the interface **net.franksreich.Manager** on path **/net/franksreich/counter1**.
The interface implements the method **AddCounter** which adds a new counter object to the service. When
the counter object is added, the application calls **sd_bus_emit_object_added** so that the object manager
emits the signal.

In addition to these changes I changed the v-table for the *Counter* object and added the flag 
**SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE** so that the introspection of the *Counter* object indicated that it
sends a **PropertiesChanged** signal when the property changes. To actually send the signal, I added a call
to **sd_bus_emit_properties_changed()** in the setter for the property and the handler for the **IncrementBy**
method.

The source code for the complete script follows below.

!INCLUDECODE "examples/example-sd-event-and-sd-bus/sd_event_loop_with_sd_bus_with_object_manager_property_signal_example.cpp" (cpp)

After I start the application, it registers itself under the path **/net/franksreich/counter1**. The `busctl`
tool allows monitoring messages on the service bus. Running the tool with
`busctl --user monitor net.franksreich.counter1` will output all messages relating to the service of the sample
application on standard output.

I use `busctl` with the following call to add a new counter.

```
busctl --user call net.franksreich.counter1 /net/franksreich/counter1 net.franksreich.Manager AddCounter u 5
```

This yields the following output on the monitoring `busctl` on standard output, indicating that the object
manager send the **InterfacesAdded** signal.

```
‣ Type=signal  Endian=l  Flags=1  Version=1  Priority=0 Cookie=3                                                                            
  Sender=:1.502  Path=/net/franksreich/counter1  Interface=org.freedesktop.DBus.ObjectManager  Member=InterfacesAdded                       
  UniqueName=:1.502                                                                                                                         
  MESSAGE "oa{sa{sv}}" {                                                                                                                    
          OBJECT_PATH "/net/franksreich/counter1/0";                                                                                        
          ARRAY "{sa{sv}}" {                                                                                                                
                  DICT_ENTRY "sa{sv}" {                                                                                                     
                          STRING "org.freedesktop.DBus.Peer";
                          ARRAY "{sv}" {
                          };
                  };
                  DICT_ENTRY "sa{sv}" {
                          STRING "org.freedesktop.DBus.Introspectable";
                          ARRAY "{sv}" {
                          };
                  };
                  DICT_ENTRY "sa{sv}" {
                          STRING "org.freedesktop.DBus.Properties";
                          ARRAY "{sv}" {
                          };
                  };
                  DICT_ENTRY "sa{sv}" {
                          STRING "org.freedesktop.DBus.ObjectManager";
                          ARRAY "{sv}" {
                          };
                  };
                  DICT_ENTRY "sa{sv}" {
                          STRING "net.franksreich.Counter";
                          ARRAY "{sv}" {
                                  DICT_ENTRY "sv" {
                                          STRING "Count";
                                          VARIANT "u" {
                                                  UINT32 5;
                                          };
                                  };
                          };
                  };
          };
  };
```

Calling the method **IncrementBy** of the counter object I just added using
`busctl --user call net.franksreich.counter1 /net/franksreich/counter1/0 net.franksreich.Counter IncrementBy u 5`
yields the following output indicating that the property changed by signalling a **PropertiesChanged**
signal.

```
‣ Type=signal  Endian=l  Flags=1  Version=1  Priority=0 Cookie=9
  Sender=:1.529  Path=/net/franksreich/counter1/0  Interface=org.freedesktop.DBus.Properties  Member=PropertiesChanged
  UniqueName=:1.529
  MESSAGE "sa{sv}as" {
          STRING "net.franksreich.Counter";
          ARRAY "{sv}" {
                  DICT_ENTRY "sv" {
                          STRING "Count";
                          VARIANT "u" {
                                  UINT32 35;
                          };
                  };
          };
          ARRAY "s" {
          };
  };
```
