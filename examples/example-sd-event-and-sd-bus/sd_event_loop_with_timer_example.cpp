#include <stdexcept>
#include <iostream>

#include <signal.h>

#include <systemd/sd-event.h>


using namespace std;

sd_event *createEventLoop() {
    int errorCode = 0;

    cout << "Creating event loop" << "\n";

    sd_event *event = nullptr;
    errorCode = sd_event_default(&event);
    if (errorCode < 0) {
        throw runtime_error("Could not create default event loop");
    }

    return event;
}

void blockSignals() {
    cout << "Clocking signals" << "\n";
    sigset_t signalSet;
    if (sigemptyset(&signalSet) < 0 ||
        sigaddset(&signalSet, SIGTERM) < 0 ||
        sigaddset(&signalSet, SIGINT))
    {
        throw runtime_error("Could not setup signal set");
    }

    if (sigprocmask(SIG_BLOCK, &signalSet, nullptr) < 0) {
        throw runtime_error("Could not block signals");
    }
}

void addSignalHandler(sd_event *event) {
    int errorCode = 0;

    cout << "Adding signal handler" << "\n";
    errorCode = sd_event_add_signal(event, nullptr, SIGTERM, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGTERM to event loop");
    }

    errorCode = sd_event_add_signal(event, nullptr, SIGINT, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGINT to event loop");
    }
}

void runEventLoop(sd_event *event) {
    int errorCode = 0;

    cout << "Starting event loop" << "\n";
    errorCode = sd_event_loop(event);
    if (errorCode < 0) {
        throw runtime_error("Error while running event loop");
    }
}

int timerCallback(sd_event_source *timerEvent, uint64_t time, void * userData) {
    cout << "Timer was triggered" << "\n";
    uint64_t wakeupTime = 0;
    sd_event *event = sd_event_source_get_event(timerEvent);
    sd_event_now(event, CLOCK_MONOTONIC, &wakeupTime);
    wakeupTime += 1000000;
    sd_event_source_set_time(timerEvent, wakeupTime);
    return 0;
}

int main(int argc, char *argv[]) {
    sd_event *event = nullptr;
    sd_event_source *timerEvent = nullptr;

    try {
        event = createEventLoop();
        blockSignals();
        addSignalHandler(event);

        uint64_t wakeupTime = 0;
        sd_event_now(event, CLOCK_MONOTONIC, &wakeupTime);
        wakeupTime += 1000000;
        sd_event_add_time(
                event, &timerEvent, CLOCK_MONOTONIC, wakeupTime, 0, &timerCallback, nullptr);
        sd_event_source_set_enabled(timerEvent, SD_EVENT_ON);

        runEventLoop(event);
    } catch (runtime_error &error) {
        cerr << error.what();
    }

    timerEvent = sd_event_source_unref(timerEvent);
    event = sd_event_unref(event);
}
